package com.clipperl.jingfen;

public class JdBean {

    /**
     * jd_union_open_order_row_query_response : {"result":"{\"code\":200,\"data\":[{\"actualCosPrice\":0.0,\"actualFee\":0.0,\"balanceExt\":\"\",\"cid1\":9847,\"cid2\":9851,\"cid3\":9881,\"commissionRate\":2.0,\"cpActId\":0,\"estimateCosPrice\":49.0,\"estimateFee\":0.69,\"ext1\":\"\",\"finalRate\":70.0,\"finishTime\":\"\",\"giftCouponKey\":\"\",\"giftCouponOcsAmount\":0.0,\"id\":\"512985925791354880\",\"modifyTime\":\"2020-09-26 13:43:56\",\"orderEmt\":2,\"orderId\":127430850965,\"orderTime\":\"2020-09-26 13:42:55\",\"parentId\":0,\"payMonth\":0,\"pid\":\"1003107252_4000258644_3002657815\",\"plus\":1,\"popId\":0,\"positionId\":3002657815,\"price\":49.0,\"proPriceAmount\":0.00,\"rid\":0,\"siteId\":4000258644,\"skuFrozenNum\":0,\"skuId\":100004189192,\"skuName\":\"家乐铭品 电脑桌增高架电脑显示器支架便携桌面屏幕托架办公室置物架键盘收纳架书架储物架子 ZC2610-S\",\"skuNum\":1,\"skuReturnNum\":0,\"subSideRate\":63.0,\"subUnionId\":\"\",\"subsidyRate\":7.0,\"traceType\":2,\"unionAlias\":\"知乎\",\"unionId\":1003107252,\"unionRole\":1,\"unionTag\":\"00000000000000000000000000000000\",\"validCode\":16}],\"hasMore\":false,\"message\":\"success\",\"requestId\":\"o_0abfbe1a_kfjirlg2_14741207\"}","code":"0"}
     */

    private JdUnionOpenOrderRowQueryResponseBean jd_union_open_order_row_query_response;

    public JdUnionOpenOrderRowQueryResponseBean getJd_union_open_order_row_query_response() {
        return jd_union_open_order_row_query_response;
    }

    public void setJd_union_open_order_row_query_response(JdUnionOpenOrderRowQueryResponseBean jd_union_open_order_row_query_response) {
        this.jd_union_open_order_row_query_response = jd_union_open_order_row_query_response;
    }

    public static class JdUnionOpenOrderRowQueryResponseBean {
        /**
         * result : {"code":200,"data":[{"actualCosPrice":0.0,"actualFee":0.0,"balanceExt":"","cid1":9847,"cid2":9851,"cid3":9881,"commissionRate":2.0,"cpActId":0,"estimateCosPrice":49.0,"estimateFee":0.69,"ext1":"","finalRate":70.0,"finishTime":"","giftCouponKey":"","giftCouponOcsAmount":0.0,"id":"512985925791354880","modifyTime":"2020-09-26 13:43:56","orderEmt":2,"orderId":127430850965,"orderTime":"2020-09-26 13:42:55","parentId":0,"payMonth":0,"pid":"1003107252_4000258644_3002657815","plus":1,"popId":0,"positionId":3002657815,"price":49.0,"proPriceAmount":0.00,"rid":0,"siteId":4000258644,"skuFrozenNum":0,"skuId":100004189192,"skuName":"家乐铭品 电脑桌增高架电脑显示器支架便携桌面屏幕托架办公室置物架键盘收纳架书架储物架子 ZC2610-S","skuNum":1,"skuReturnNum":0,"subSideRate":63.0,"subUnionId":"","subsidyRate":7.0,"traceType":2,"unionAlias":"知乎","unionId":1003107252,"unionRole":1,"unionTag":"00000000000000000000000000000000","validCode":16}],"hasMore":false,"message":"success","requestId":"o_0abfbe1a_kfjirlg2_14741207"}
         * code : 0
         */

        private String result;
        private String code;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
