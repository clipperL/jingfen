package com.clipperl.jingfen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 收到广播，重新执行service
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, LongRunningService.class);
        context.startService(i);
    }
}
