package com.clipperl.jingfen;

import java.util.List;

public class JdInBean {
    /**
     * code : 200
     * data : [{"actualCosPrice":0,"actualFee":0,"balanceExt":"","cid1":9847,"cid2":9851,"cid3":9881,"commissionRate":2,"cpActId":0,"estimateCosPrice":49,"estimateFee":0.69,"ext1":"","finalRate":70,"finishTime":"","giftCouponKey":"","giftCouponOcsAmount":0,"id":"512985925791354880","modifyTime":"2020-09-26 13:43:56","orderEmt":2,"orderId":127430850965,"orderTime":"2020-09-26 13:42:55","parentId":0,"payMonth":0,"pid":"1003107252_4000258644_3002657815","plus":1,"popId":0,"positionId":3002657815,"price":49,"proPriceAmount":0,"rid":0,"siteId":4000258644,"skuFrozenNum":0,"skuId":100004189192,"skuName":"家乐铭品 电脑桌增高架电脑显示器支架便携桌面屏幕托架办公室置物架键盘收纳架书架储物架子 ZC2610-S","skuNum":1,"skuReturnNum":0,"subSideRate":63,"subUnionId":"","subsidyRate":7,"traceType":2,"unionAlias":"知乎","unionId":1003107252,"unionRole":1,"unionTag":"00000000000000000000000000000000","validCode":16}]
     * hasMore : false
     * message : success
     * requestId : o_0abfbe1a_kfjirlg2_14741207
     */

    private int code;
    private boolean hasMore;
    private String message;
    private String requestId;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * actualCosPrice : 0
         * actualFee : 0
         * balanceExt :
         * cid1 : 9847
         * cid2 : 9851
         * cid3 : 9881
         * commissionRate : 2
         * cpActId : 0
         * estimateCosPrice : 49
         * estimateFee : 0.69
         * ext1 :
         * finalRate : 70
         * finishTime :
         * giftCouponKey :
         * giftCouponOcsAmount : 0
         * id : 512985925791354880
         * modifyTime : 2020-09-26 13:43:56
         * orderEmt : 2
         * orderId : 127430850965
         * orderTime : 2020-09-26 13:42:55
         * parentId : 0
         * payMonth : 0
         * pid : 1003107252_4000258644_3002657815
         * plus : 1
         * popId : 0
         * positionId : 3002657815
         * price : 49
         * proPriceAmount : 0
         * rid : 0
         * siteId : 4000258644
         * skuFrozenNum : 0
         * skuId : 100004189192
         * skuName : 家乐铭品 电脑桌增高架电脑显示器支架便携桌面屏幕托架办公室置物架键盘收纳架书架储物架子 ZC2610-S
         * skuNum : 1
         * skuReturnNum : 0
         * subSideRate : 63
         * subUnionId :
         * subsidyRate : 7
         * traceType : 2
         * unionAlias : 知乎
         * unionId : 1003107252
         * unionRole : 1
         * unionTag : 00000000000000000000000000000000
         * validCode : 16
         */

        private float actualCosPrice;
        private float actualFee;
        private String balanceExt;
        private long cid1;
        private long cid2;
        private long cid3;
        private float commissionRate;
        private long cpActId;
        private float estimateCosPrice;
        private double estimateFee;
        private String ext1;
        private float finalRate;
        private String finishTime;
        private String giftCouponKey;
        private float giftCouponOcsAmount;
        private String id;
        private String modifyTime;
        private int orderEmt;
        private long orderId;
        private String orderTime;
        private long parentId;
        private int payMonth;
        private String pid;
        private int plus;
        private long popId;
        private long positionId;
        private float price;
        private float proPriceAmount;
        private int rid;
        private long siteId;
        private int skuFrozenNum;
        private long skuId;
        private String skuName;
        private int skuNum;
        private int skuReturnNum;
        private float subSideRate;
        private String subUnionId;
        private float subsidyRate;
        private int traceType;
        private String unionAlias;
        private long unionId;
        private int unionRole;
        private String unionTag;
        private int validCode;

        public float getActualCosPrice() {
            return actualCosPrice;
        }

        public void setActualCosPrice(float actualCosPrice) {
            this.actualCosPrice = actualCosPrice;
        }

        public float getActualFee() {
            return actualFee;
        }

        public void setActualFee(float actualFee) {
            this.actualFee = actualFee;
        }

        public String getBalanceExt() {
            return balanceExt;
        }

        public void setBalanceExt(String balanceExt) {
            this.balanceExt = balanceExt;
        }

        public long getCid1() {
            return cid1;
        }

        public void setCid1(long cid1) {
            this.cid1 = cid1;
        }

        public long getCid2() {
            return cid2;
        }

        public void setCid2(long cid2) {
            this.cid2 = cid2;
        }

        public long getCid3() {
            return cid3;
        }

        public void setCid3(long cid3) {
            this.cid3 = cid3;
        }

        public float getCommissionRate() {
            return commissionRate;
        }

        public void setCommissionRate(float commissionRate) {
            this.commissionRate = commissionRate;
        }

        public long getCpActId() {
            return cpActId;
        }

        public void setCpActId(long cpActId) {
            this.cpActId = cpActId;
        }

        public float getEstimateCosPrice() {
            return estimateCosPrice;
        }

        public void setEstimateCosPrice(float estimateCosPrice) {
            this.estimateCosPrice = estimateCosPrice;
        }

        public double getEstimateFee() {
            return estimateFee;
        }

        public void setEstimateFee(double estimateFee) {
            this.estimateFee = estimateFee;
        }

        public String getExt1() {
            return ext1;
        }

        public void setExt1(String ext1) {
            this.ext1 = ext1;
        }

        public float getFinalRate() {
            return finalRate;
        }

        public void setFinalRate(float finalRate) {
            this.finalRate = finalRate;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getGiftCouponKey() {
            return giftCouponKey;
        }

        public void setGiftCouponKey(String giftCouponKey) {
            this.giftCouponKey = giftCouponKey;
        }

        public float getGiftCouponOcsAmount() {
            return giftCouponOcsAmount;
        }

        public void setGiftCouponOcsAmount(float giftCouponOcsAmount) {
            this.giftCouponOcsAmount = giftCouponOcsAmount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getModifyTime() {
            return modifyTime;
        }

        public void setModifyTime(String modifyTime) {
            this.modifyTime = modifyTime;
        }

        public int getOrderEmt() {
            return orderEmt;
        }

        public void setOrderEmt(int orderEmt) {
            this.orderEmt = orderEmt;
        }

        public long getOrderId() {
            return orderId;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public String getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(String orderTime) {
            this.orderTime = orderTime;
        }

        public long getParentId() {
            return parentId;
        }

        public void setParentId(long parentId) {
            this.parentId = parentId;
        }

        public int getPayMonth() {
            return payMonth;
        }

        public void setPayMonth(int payMonth) {
            this.payMonth = payMonth;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public int getPlus() {
            return plus;
        }

        public void setPlus(int plus) {
            this.plus = plus;
        }

        public long getPopId() {
            return popId;
        }

        public void setPopId(long popId) {
            this.popId = popId;
        }

        public long getPositionId() {
            return positionId;
        }

        public void setPositionId(long positionId) {
            this.positionId = positionId;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public float getProPriceAmount() {
            return proPriceAmount;
        }

        public void setProPriceAmount(float proPriceAmount) {
            this.proPriceAmount = proPriceAmount;
        }

        public int getRid() {
            return rid;
        }

        public void setRid(int rid) {
            this.rid = rid;
        }

        public long getSiteId() {
            return siteId;
        }

        public void setSiteId(long siteId) {
            this.siteId = siteId;
        }

        public int getSkuFrozenNum() {
            return skuFrozenNum;
        }

        public void setSkuFrozenNum(int skuFrozenNum) {
            this.skuFrozenNum = skuFrozenNum;
        }

        public long getSkuId() {
            return skuId;
        }

        public void setSkuId(long skuId) {
            this.skuId = skuId;
        }

        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
        }

        public int getSkuNum() {
            return skuNum;
        }

        public void setSkuNum(int skuNum) {
            this.skuNum = skuNum;
        }

        public int getSkuReturnNum() {
            return skuReturnNum;
        }

        public void setSkuReturnNum(int skuReturnNum) {
            this.skuReturnNum = skuReturnNum;
        }

        public float getSubSideRate() {
            return subSideRate;
        }

        public void setSubSideRate(float subSideRate) {
            this.subSideRate = subSideRate;
        }

        public String getSubUnionId() {
            return subUnionId;
        }

        public void setSubUnionId(String subUnionId) {
            this.subUnionId = subUnionId;
        }

        public float getSubsidyRate() {
            return subsidyRate;
        }

        public void setSubsidyRate(float subsidyRate) {
            this.subsidyRate = subsidyRate;
        }

        public int getTraceType() {
            return traceType;
        }

        public void setTraceType(int traceType) {
            this.traceType = traceType;
        }

        public String getUnionAlias() {
            return unionAlias;
        }

        public void setUnionAlias(String unionAlias) {
            this.unionAlias = unionAlias;
        }

        public long getUnionId() {
            return unionId;
        }

        public void setUnionId(long unionId) {
            this.unionId = unionId;
        }

        public int getUnionRole() {
            return unionRole;
        }

        public void setUnionRole(int unionRole) {
            this.unionRole = unionRole;
        }

        public String getUnionTag() {
            return unionTag;
        }

        public void setUnionTag(String unionTag) {
            this.unionTag = unionTag;
        }

        public int getValidCode() {
            return validCode;
        }

        public void setValidCode(int validCode) {
            this.validCode = validCode;
        }
    }
}
