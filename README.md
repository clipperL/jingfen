# JingFen

#### 介绍
Notification of order receipt

运行前，请将你自己京粉的 appkey 和 secretkey 填入 gradle.properties 中指定位置；

说明一点，为什么没把 appkey 和 secretkey 做成两个输入框，方便大家直接下载成 apk 使用；
因为害怕有人担心我会偷偷窃取大家的 appkey 和 secretkey，所以我放弃了这种便利的方式，只是为了让使用者下载整个代码，可以明白地看到自己的 appkey 和 secretkey 用在了哪个地方。

![jf.png](https://gitee.com/clipperL/jingfen/raw/master/app/src/main/assets/key.png)

通知的样式如下：

![image.png](https://gitee.com/clipperL/jingfen/raw/master/app/src/main/assets/notify.png)

# 注意事项：

1、请在手机上打开该 App 的通知权限和开关；

2、目前的设定是每隔 10 分钟会发起一起网络请求，请求 10 分钟内是否有订单；
晚上睡觉前，请将该软件杀后台，防止晚上发通知打扰到你。

3、目前该 App 保活机制没有做到位，很有可能会被系统杀死，可以尝试在低版本的手机上使用，或者找一个不用的手机专门在前台运行该 App（我尝试在华为手机[Android10]的系统白名单中增加该 App，但还是失败了）；



# TODO，将来要增加的功能；

- [ ] 加入修改网络请求时间间隔的入口
- [ ] 加入网络请求时间段的入口，如每天07:00--22:00 才会请求网络；